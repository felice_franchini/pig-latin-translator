package piglatintranslator;

public class Translator {

	public static final String NIL = "nil";
	
	private static final String VOWELS = "aeiouAEIOU";
	private static final String CONSONANTS = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
	private static final String PUNCTUATIONS = ".,;:?!'()";
	
	private String phrase;

	public Translator(String phrase) {
		this.phrase = phrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		
		if(phrase.equals("")) return NIL;
		
		String [] words = phrase.split(" ");
		StringBuilder translation = new StringBuilder("");
		
		for (String word : words) {
			
			if(word.contains("-")) {
				
				for(String dashedWord : word.split("-")) {
					
					String[] startPunctuation = removeStartPunctuation(dashedWord);
					String[] endPunctuation = removeEndPunctuation(startPunctuation[0]);
					
					translation.append(startPunctuation[1] + translateWord(endPunctuation[0]) + endPunctuation[1] + "-");
					
				}
				
				translation.deleteCharAt(translation.length()-1);
				
			} else {

				String[] startPunctuation = removeStartPunctuation(word);
				String[] endPunctuation = removeEndPunctuation(startPunctuation[0]);
				
				translation.append(startPunctuation[1] + translateWord(endPunctuation[0]) + endPunctuation[1]);
				
			}
			
			translation.append(" ");
			
		}
		
		return translation.deleteCharAt(translation.length()-1).toString();
					
	}
	
	private StringBuilder translateWord(String word) {
		
		StringBuilder translation = new StringBuilder("");
		
		if(startsWithVowel(word)) {
			
			if(word.endsWith("y"))  translation.append(word + "nay");
			
			else if(endsWithVowel(word)) translation.append(word + "yay");
			
			else translation.append(phrase + "ay");
			
		} else {
			
			String phraseWithoutFirstsConsonants = word;
			StringBuilder firstsConsonants = new StringBuilder("");
			
			for(int consideringCharacter = 0; 
					consideringCharacter < word.length() - 1 && isConsonant(word.charAt(consideringCharacter)); 
					consideringCharacter++) {
				
				firstsConsonants.append(phraseWithoutFirstsConsonants.charAt(0));
				phraseWithoutFirstsConsonants = phraseWithoutFirstsConsonants.substring(1);
				
			}
			
			translation.append(phraseWithoutFirstsConsonants + firstsConsonants + "ay");
			
		}
		
		return translation;
		
	}
	
	private String[] removeStartPunctuation(String word) {
		
		String punctuations = "";
		
		while(isPunctuation(word.charAt(0))) {
			
			punctuations += word.charAt(0);
			word = word.substring(1);
			
		}
		
		return new String[] {word, punctuations};
	}
	
	private String[] removeEndPunctuation(String word) {
		
		String punctuations = "";
		int analyzedChar = word.length()-1;
		
		while(isPunctuation(word.charAt(analyzedChar))) {
			
			punctuations = word.charAt(analyzedChar) + punctuations;
			word = word.substring(0, word.length() - 1);
			analyzedChar--;
			
		}
		
		return new String[] {word, punctuations};
	}
	
	private boolean startsWithVowel(String word) {
		 return isVowel(word.charAt(0));
	}	
	
	private boolean endsWithVowel(String word) {
		return isVowel(word.charAt(word.length() - 1));
	}
	
	private boolean isVowel(char c) {
		return VOWELS.contains(Character.toString(c));
	}
	
	private boolean isConsonant(char c) {
	    return CONSONANTS.contains(Character.toString(c));
	}
	
	private boolean isPunctuation(char c) {
		return PUNCTUATIONS.contains(Character.toString(c));
	}

}
