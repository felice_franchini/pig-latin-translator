package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;


public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test 
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test 
	public void testTranslationWordStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationWordStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test 
	public void testTranslationWordStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}

	@Test 
	public void testTranslationWordStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test 
	public void testTranslationWordStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test 
	public void testTranslationWordStartingWithMultipleConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test 
	public void testTranslationPhraseWithWordsSeparatedByWhiteSpace() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test 
	public void testTranslationPhraseWithWordsSeparatedByDash() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test 
	public void testTranslationPhraseContainingPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test 
	public void testTranslationPhraseContainingParentesis() {
		String inputPhrase = "(hello world!)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellohay orldway!)", translator.translate());
	}
	
	@Test 
	public void testTranslationPhraseWithWordsSeparatedByDashContainingParentesis() {
		String inputPhrase = "(well-being!)";
		Translator translator = new Translator(inputPhrase);
		assertEquals("(ellway-eingbay!)", translator.translate());
	}
}
